package com.aisile.search.service.impl;

import com.aisile.common.Constant;
import com.aisile.pojo.TbItem;
import com.aisile.search.service.ItemSearchService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 要想成功, 必须强大!
 * @Title: ItenSearchServiceImpl
 * @ProjectName aisile-parent
 * @date 2018/8/23 0023
 */
@Service(timeout = 10000)
public class ItenSearchServiceImpl implements ItemSearchService{

    @Autowired
    private SolrTemplate solrTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Map<String, Object> search(Map searchMap) {
        Map<String,Object> map=new HashMap<>();
        String keywords = (String) searchMap.get("keywords");
        searchMap.put("keywords", keywords.replace(" ", ""));
        //列表
        map.putAll(searchList(searchMap));//高光操作
        //对分类进行分组 操作 如手机 电视
        map.putAll(searchCategory(searchMap));
       List<String> categoryList=(List<String>)map.get("categoryList");

        String category=(String)searchMap.get("category");
        if(!category.equals("")){
            //对分类进行分组 操作 如手机 电视
            map.putAll(searchBrandAndSpec(category));
        }else {
            //对 品牌规格
            if (categoryList!=null&&categoryList.size()>0) {
                map.putAll(searchBrandAndSpec(categoryList.get(0)));
            }
        }


      /* if (categoryList!=null&&categoryList.size()>0){
          map.putAll(searchBrandAndSpec(categoryList.get(0)));
       }*/
        return map;
    }

    /**
     * 根据高光查询
     * @param searchMap
     * @return
     */
    private Map<String,Object> searchList(Map searchMap){
        Map map=new HashMap();

        HighlightQuery query=new SimpleHighlightQuery();//设置高亮选项
        HighlightOptions highlightOptions=new HighlightOptions().addField("item_title");//设置高亮的域
        highlightOptions.setSimplePrefix("<em style='color:red'>");
        highlightOptions.setSimplePostfix("</em>");//设置显示的颜色
        query.setHighlightOptions(highlightOptions);//高亮条件

        //获取搜索条件
        String keywords=  searchMap.get("keywords")==null?"": searchMap.get("keywords").toString();
        //按照 条件查询
        Criteria criteria=new Criteria("item_keywords").is(keywords);
        //这是得到一个高光的集合 但是没有加效果
        query.addCriteria(criteria);

        //1.2按分类筛选
        if(!"".equals(searchMap.get("category"))){
            Criteria filterCriteria=new Criteria("item_category").is(searchMap.get("category"));
            FilterQuery filterQuery=new SimpleFilterQuery(filterCriteria);
            query.addFilterQuery(filterQuery);
        }


        //1.3按品牌筛选
        if(!"".equals(searchMap.get("brand"))){
            Criteria filterCriteria=new Criteria("item_brand").is(searchMap.get("brand"));
            FilterQuery filterQuery=new SimpleFilterQuery(filterCriteria);
            query.addFilterQuery(filterQuery);
        }

        //按照规格的过滤  因为  spec是个对象1.4
        if(searchMap.get("spec")!=null){
            Map<String,String> specMap= (Map) searchMap.get("spec");
            for(String key:specMap.keySet() ){
                System.out.println(key);
                Criteria filterCriteria=new Criteria("item_spec_"+key).is( specMap.get(key) );
                FilterQuery filterQuery=new SimpleFilterQuery(filterCriteria);
                query.addFilterQuery(filterQuery);
            }
        }

        //1.5价格
        if (!"".equals(searchMap.get("price"))){
            String [] price=((String)searchMap.get("price")).split("-");
            if (!price[0].equals("0")) { //如果价格不等于0                         大于等于
                Criteria filterCriteria=new Criteria("item_price").greaterThanEqual(price[0]);
                FilterQuery filterQuery=new SimpleFilterQuery(filterCriteria);
                query.addFilterQuery(filterQuery);
            }

            if(!price[1].equals("*")){//如果区间终点不等于*
                Criteria filterCriteria=new  Criteria("item_price").lessThanEqual(price[1]);
                FilterQuery filterQuery=new SimpleFilterQuery(filterCriteria);
                query.addFilterQuery(filterQuery);
            }
        }
        //1.6分页
        Integer pageNo= Integer.parseInt (searchMap.get("pageNo").toString());//提取页码
        if(pageNo==null){
            pageNo=1;//默认第一页
        }
        String pageSizestr=searchMap.get("pageSize").toString();//每页记录数
        Integer pageSize=   Integer.valueOf(pageSizestr);
        System.out.println(pageSize);
        if(pageSize==null){
            pageSize=20;//默认20
        }
        query.setOffset((pageNo-1)*pageSize);//计算 想要的页数
        query.setRows(pageSize);//没有记录数

        //1.7排序 价格
        String sortValue = (String) searchMap.get("sort");//排序的规则asc 升 desc降序
        String  sortField=(String) searchMap.get("sortField");//排序的字段
        if (sortValue!=null&!sortValue.equals("")){
            if (sortValue.equals("ASC")){
                Sort sort=new Sort(Sort.Direction.ASC,"item_"+sortField);//升序
                query.addSort(sort);
            }
            if (sortValue.equals("DESC")){
                Sort sort=new Sort(Sort.Direction.DESC,"item_"+sortField);//升序
                query.addSort(sort);
            }
        }






        /*******     执行高亮操作 获取结果集   *********/
        HighlightPage<TbItem> page = solrTemplate.queryForHighlightPage(query, TbItem.class);

        List<HighlightEntry<TbItem>> highlighted = page.getHighlighted();
        for (HighlightEntry<TbItem> entity:highlighted){
            TbItem item=entity.getEntity();//原先的每组数据
            if (entity.getHighlights()!=null&&entity.getHighlights().size()>0){
                HighlightEntry.Highlight highlight = entity.getHighlights().get(0);
                String title = highlight.getSnipplets().get(0);
                item.setTitle(title);
            }

        }
        map.put("rows",page.getContent());

        map.put("totalPages",page.getTotalPages());//页数
        map.put("total",page.getTotalPages());//返回总记录数

        return  map;
    }


    /**
     *进行分类 且分组
     */
    private  Map<String,Object> searchCategory(Map searchMap){
        Map map=new HashMap();
        Query query=new SimpleQuery();
        String keywords=  searchMap.get("keywords")==null?"": searchMap.get("keywords").toString();
        Criteria criteria=new Criteria("item_keywords").is(keywords);
        //这是搜索的条件
        query.addCriteria(criteria);//加入条件
        //创建一个分组
        GroupOptions groupOptions=new GroupOptions();
        //为分组 创建 一个分组的条件
        groupOptions.addGroupByField("item_category");
        query.setGroupOptions(groupOptions);
        //进行条件查询 初步分组
        GroupPage<TbItem> page = solrTemplate.queryForGroupPage(query, TbItem.class);//得到分组的结果

        GroupResult<TbItem> groupResult = page.getGroupResult("item_category");
        Page<GroupEntry<TbItem>> groupEntries = groupResult.getGroupEntries();
        //获取 分组后的数据
        List<String> list=new ArrayList<>();
        for (GroupEntry<TbItem> group:groupEntries){
                list.add(group.getGroupValue());
        }
        map.put("categoryList",list);
        return  map;
    }


    /**
     * 根据分类
     * 查询对应的 品牌列表 规格列表
     * @param category
     * @return
     */
    private  Map<String,Object> searchBrandAndSpec(String category){
        Map<String,Object> map=new HashMap<>();
        //去redis中获取 模板id
        Long id = (Long) redisTemplate.boundHashOps(Constant.ITEMCAT_TEMPLATE_CATE).get(category);
        //根据模板id 去redis中查询 表格中的品牌
      List<Map> brandList=(List<Map>)redisTemplate.boundHashOps(Constant.TEMPLATE_BRAND_CACHE).get(id);
      //根据模板id 去redis中查询 表格中的规格
       List<Map> spcList=(List<Map>) redisTemplate.boundHashOps(Constant.TEMPLATE_SPEC_CACHE).get(id);
       map.put("brandList",brandList);
       map.put("specList",spcList);
       return  map;
    }

    @Override
    public void importList(List list) {
        solrTemplate.saveBeans(list);
        solrTemplate.commit();
    }

    /**
     * 删除功能
     * @param goodsIds
     */

    @Override
    public void deleteByGoodsIds(List goodsIds) {

        Query query=new SimpleQuery("");
        Criteria criteria=new Criteria("item_goodsid").in(goodsIds);
        query.addCriteria(criteria);
        solrTemplate.delete(query);
        solrTemplate.commit();
    }
}
