package com.aisile.search.service.impl;

import com.aisile.search.service.ItemSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.util.Arrays;

/**
 * @author 要想成功, 必须强大!
 * @Title: ItemDeleteListener
 * @ProjectName aisile-parent
 * @date 2018/8/31 0031
 */
@Component
public class ItemDeleteListener implements MessageListener {
    @Autowired
    private ItemSearchService itemSearchService;
    @Override
    public void onMessage(Message message) {
        try {
            //转化
            ObjectMessage objectMessage=(ObjectMessage) message;
            Long [] ids=(Long []) objectMessage.getObject();
            System.out.println("这是要删除的信息:"+ids);
            itemSearchService.deleteByGoodsIds(Arrays.asList(ids));
            System.out.println("删除完毕!");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
