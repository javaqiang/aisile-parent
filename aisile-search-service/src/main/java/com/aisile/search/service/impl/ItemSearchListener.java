package com.aisile.search.service.impl;

import com.aisile.pojo.TbItem;
import com.aisile.search.service.ItemSearchService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.List;
import java.util.Map;


/**
 * @author 要想成功, 必须强大!
 * @Title: ItemSearchListener
 * @ProjectName aisile-parent
 * @date 2018/8/31 0031
 */
@Component
public class ItemSearchListener implements MessageListener {

    @Autowired
    ItemSearchService itemSearchService;

    @Override
    public void onMessage(Message message) {
        System.out.println("收到的消息!");
        TextMessage textMessage= (TextMessage) message;
        try {
            String text=textMessage.getText();
            System.out.println("消息是:"+text);
            List<TbItem> itemList = JSON.parseArray(text, TbItem.class);
            for (TbItem item:itemList){
                Map specMap= JSON.parseObject(item.getSpec());//将spec字段中的json字符串转换为map
                item.setSpecMap(specMap);//给带注解的字段赋值
            }
            //导入sort库
            itemSearchService.importList(itemList);
            System.out.println("更新索引成功!");

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
