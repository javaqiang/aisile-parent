package com.aisile.content.service.impl;


import com.aisile.common.Constant;
import com.aisile.content.service.ContentService;
import com.aisile.entry.PageResult;
import com.aisile.mapper.TbContentMapper;
import com.aisile.pojo.TbContent;
import com.aisile.pojo.TbContentExample;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class ContentServiceImpl implements ContentService {

	@Autowired
	private TbContentMapper contentMapper;
	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbContent> findAll() {
		return contentMapper.selectByExample(null);
	}

	/**
	 * 按分页查询
	 */
	@Override
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<TbContent> page=   (Page<TbContent>) contentMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}

	/**
	 * 增加
	 */
	@Override
	public void add(TbContent content) {
		contentMapper.insert(content);
		//增加后删除redis
		redisTemplate.boundHashOps(Constant.CONTENT_CACHE).delete(content.getCategoryId());
	}

	
	/**
	 * 修改
	 */
	@Override
	public void update(TbContent content){

		//查询
		Long categoryId = contentMapper.selectByPrimaryKey(content.getId()).getCategoryId();
		//清除redis
		redisTemplate.boundHashOps(Constant.CONTENT_CACHE).delete(categoryId);

		contentMapper.updateByPrimaryKey(content);
		//如果分类ID发生了修改,清除修改后的分类ID的缓存
		if (categoryId.longValue()!=content.getCategoryId().longValue()){
			redisTemplate.boundHashOps(Constant.CONTENT_CACHE).delete(content.getCategoryId());
		}
	}	
	
	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbContent findOne(Long id){
		return contentMapper.selectByPrimaryKey(id);
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
			contentMapper.deleteByPrimaryKey(id);

			//查询 广告分类id
			Long getCategoryId=contentMapper.selectByPrimaryKey(id).getCategoryId();
			//根据 id删除 redis中的广告
			redisTemplate.boundHashOps(Constant.CONTENT_CACHE).delete(getCategoryId);

		}		
	}
	
	
		@Override
	public PageResult findPage(TbContent content, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		
		TbContentExample example=new TbContentExample();
		TbContentExample.Criteria criteria = example.createCriteria();
		
		if(content!=null){			
						if(content.getTitle()!=null && content.getTitle().length()>0){
				criteria.andTitleLike("%"+content.getTitle()+"%");
			}
			if(content.getUrl()!=null && content.getUrl().length()>0){
				criteria.andUrlLike("%"+content.getUrl()+"%");
			}
			if(content.getPic()!=null && content.getPic().length()>0){
				criteria.andPicLike("%"+content.getPic()+"%");
			}
			if(content.getStatus()!=null && content.getStatus().length()>0){
				criteria.andStatusLike("%"+content.getStatus()+"%");
			}
	
		}
		
		Page<TbContent> page= (Page<TbContent>)contentMapper.selectByExample(example);		
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Autowired
	private RedisTemplate redisTemplate;
    @Override
    public List<TbContent> findByCategoryId(Long categoryId) {
    	//首先去redis查询
		List<TbContent> contentlist =(List<TbContent>) redisTemplate.boundHashOps(Constant.CONTENT_CACHE).get(categoryId);
		if (contentlist!=null&&contentlist.size()>0){
			return  contentlist;
		}else{
			//查不到 的话 就从数据库查 查到并且添加到redis
			TbContentExample example=new TbContentExample();
			TbContentExample.Criteria criteria=example.createCriteria();
			criteria.andCategoryIdEqualTo(categoryId);
			criteria.andStatusEqualTo("1");
			example.setOrderByClause("sort_order");
			List<TbContent> contents = contentMapper.selectByExample(example);
			//添加到缓存
			redisTemplate.boundHashOps(Constant.CONTENT_CACHE).put(categoryId,contents);
			return contents;
		}


    }
}
