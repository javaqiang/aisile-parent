package com.aisile.page.service.impl;

import com.aisile.mapper.TbGoodsDescMapper;
import com.aisile.mapper.TbGoodsMapper;
import com.aisile.mapper.TbItemCatMapper;
import com.aisile.mapper.TbItemMapper;
import com.aisile.page.service.ItemPageService;
import com.aisile.pojo.TbGoods;
import com.aisile.pojo.TbGoodsDesc;
import com.aisile.pojo.TbItem;
import com.aisile.pojo.TbItemExample;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 要想成功, 必须强大!
 * @Title: ItemPageServiceImpl
 * @ProjectName aisile-parent
 * @date 2018/8/29 0029
 */
@Service
public class ItemPageServiceImpl implements ItemPageService{
    @Value("${pagePath}")
    String pagePath; //页面生成的地址

    @Autowired
    private FreeMarkerConfigurer freemarkerConfig; //freemark 对象 spring提供的

    @Autowired
    TbGoodsMapper tbGoodsMapper; //查询商品信息

    @Autowired
    TbGoodsDescMapper tbGoodsDescMapper;

    @Autowired
    private TbItemCatMapper itemCatMapper;//面包屑

    @Autowired
    private TbItemMapper itemMapper;


    /**
     *这是页面生成
     * @param goodsId
     * @return
     */
    @Override
    public boolean genItemHtml(Long goodsId) {

        //模板
        Configuration configuration = freemarkerConfig.getConfiguration();
        try {
           Template template= configuration.getTemplate("item.ftl");
            //数据 spu
            TbGoods goods = tbGoodsMapper.selectByPrimaryKey(goodsId);
            TbGoodsDesc goodsDesc = tbGoodsDescMapper.selectByPrimaryKey(goodsId);



            Map map=new HashMap();
            map.put("goods",goods);
            map.put("goodsDesc",goodsDesc);

            //3.商品分类
            String itemCat1 = itemCatMapper.selectByPrimaryKey(goods.getCategory1Id()).getName();
            String itemCat2 = itemCatMapper.selectByPrimaryKey(goods.getCategory2Id()).getName();
            String itemCat3 = itemCatMapper.selectByPrimaryKey(goods.getCategory3Id()).getName();
            map.put("itemCat1", itemCat1);
            map.put("itemCat2", itemCat2);
            map.put("itemCat3", itemCat3);

            //4、获取sku表
            TbItemExample example=new TbItemExample();
            TbItemExample.Criteria criteria = example.createCriteria();
            criteria.andGoodsIdEqualTo(goodsId); //商品id 条件
            criteria.andStatusEqualTo("1");//审核通过的
            example.setOrderByClause("is_default desc");//查看是否默认字段进行排序 目的降序
            List<TbItem> itemList = itemMapper.selectByExample(example);
            map.put("itemList",itemList);
            //输出节点
            Writer writer=new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(pagePath+goodsId+".html"),"utf-8"));
           template.process(map,writer); //生成页面
           writer.close();
            return  true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }





    }

    /**
     * 删除页面
     * @param goodsIds
     * @return
     */
    @Override
    public boolean deleteItemHtml(Long[] goodsIds) {
        try {
            for (Long good:goodsIds){
                new File(pagePath+good+".html");
            }
            return  true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
}
