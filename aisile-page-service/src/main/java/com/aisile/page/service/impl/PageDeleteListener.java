package com.aisile.page.service.impl;

import com.aisile.page.service.ItemPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * @author 要想成功, 必须强大!
 * @Title: PageDeleteListener
 * @ProjectName aisile-parent
 * @date 2018/8/31 0031
 */
@Component
public class PageDeleteListener implements MessageListener{

    @Autowired
    private ItemPageService itemPageService;

    @Override
    public void onMessage(Message message) {
        try {
            ObjectMessage objectMessage=(ObjectMessage) message;
            Long [] goodsIds=(Long [])objectMessage.getObject();
            System.out.println("订阅删除页面:"+goodsIds);
            boolean b=itemPageService.deleteItemHtml(goodsIds);
            System.out.println("结果:"+b);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
