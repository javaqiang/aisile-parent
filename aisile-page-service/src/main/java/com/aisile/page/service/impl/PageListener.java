package com.aisile.page.service.impl;

import com.aisile.page.service.ItemPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * @author 要想成功, 必须强大!
 * @Title: PageListener
 * @ProjectName aisile-parent
 * @date 2018/8/31 0031
 */
@Component
public class PageListener implements MessageListener{

    @Autowired
    ItemPageService itemPageService;
    @Override
    public void onMessage(Message message) {
        TextMessage textMessage=(TextMessage) message;
        try {
            String text=textMessage.getText();
            System.out.println("收到的生成页面id:"+text);
            boolean b=itemPageService.genItemHtml(Long.valueOf(text));
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
