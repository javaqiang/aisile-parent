package com.aisile.cart.service.impl;

import com.aisile.cart.service.CartService;
import com.aisile.common.Constant;
import com.aisile.mapper.TbItemMapper;
import com.aisile.pojo.TbItem;
import com.aisile.pojo.TbOrderItem;
import com.aisile.pojogroup.Cart;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: CartServiceImpl
 * @ProjectName aisile-parent
 * @date 2018/9/5 0005
 */
@Service
public class CartServiceImpl implements CartService{

    @Autowired
    private TbItemMapper tbItemMapper;

    @Override
    public List<Cart> addGoodsToCartList(List<Cart> cartList, Long itemId, Integer num) {
        //1、先查询 sku 根据sku索引数据
        TbItem tbItem = tbItemMapper.selectByPrimaryKey(itemId);
        //判断 sku是否查到
        if (tbItem==null){
            throw  new RuntimeException("商品不存在");
        }
        if (!"1".equals(tbItem.getStatus())){
            throw  new RuntimeException("商品状态不合法!");
        }
        //2、获取商家id
        String sellerId = tbItem.getSellerId();
        //3、判断商家在购物车中是否存在
        Cart cart = searchCartBySellerId(cartList, sellerId);
        if (cart==null){
            //如果不存在 那么就加入
            cart=new Cart();
            cart.setSellerId(sellerId); //设置商家id
            cart.setSellerName(tbItem.getSeller()); //商家名字
            List<TbOrderItem> orderItemList =new ArrayList<>();
            TbOrderItem tbOrderItem=createOrderItem(tbItem,num);
            orderItemList.add(tbOrderItem);
            cart.setOrderItemList(orderItemList);
            cartList.add(cart);
        }else{
            //3.1、判断商品是否存在
            List<TbOrderItem> tbOrderItemList = cart.getOrderItemList();

            TbOrderItem tbOrderItem = searchItemByItemId(tbOrderItemList, itemId);
            //如果sku不存在的话 就添加
            if (tbOrderItem==null){
                //添加数据
                tbOrderItem = createOrderItem(tbItem, num);
                tbOrderItemList.add(tbOrderItem);
            }else{
                //存在的话 改变数量 价格
                tbOrderItem.setNum(tbOrderItem.getNum()+num);
                tbOrderItem.setTotalFee(new BigDecimal(tbOrderItem.getPrice().doubleValue()*tbOrderItem.getNum()));

                if (tbOrderItem.getNum()<=0){
                    tbOrderItemList.remove(tbOrderItem);
                }

                //商家列表中没有数据时 就删除
                if(tbOrderItemList.size()==0){
                    cartList.remove(cart);
                }
            }

        }

        return cartList;
    }

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 如果登录查询redis
     * @param username
     * @return
     */
    @Override
    public List<Cart> findCartListFromRedis(String username) {
        System.out.println("这是redis的数据:"+username);
        List<Cart> cartList= (List<Cart>) redisTemplate.boundHashOps(Constant.CART_COOKIE_NAME).get(username);
        //判断是否为空
        if (cartList==null){
            cartList=new ArrayList<>();
        }
        return cartList;
    }

    /**
     * 登录成功后 存储redis
     * @param username
     * @param cartList
     */
    @Override
    public void saveCartListToRedis(String username, List<Cart> cartList) {
        System.out.println("向redis存入购物车数据....."+username);
        redisTemplate.boundHashOps("cartList").put(username, cartList);
    }

    /**
     * 根据sku id判断购物车列表中是否存在
     * @param tbOrderItems
     * @param itemId
     * @return
     */
    private TbOrderItem searchItemByItemId(List<TbOrderItem> tbOrderItems,Long itemId){
        for (TbOrderItem item:tbOrderItems){
            if (itemId.longValue()==item.getItemId().longValue()){
                return  item;
            }
        }
        return  null;
    }


    /**
     * 创建订单明细
     * @param tbItem
     * @param num
     * @return
     */
    private TbOrderItem createOrderItem(TbItem tbItem,Integer num){
        if(num<=0){
            throw new RuntimeException("数量非法");
        }
        TbOrderItem tbOrderItem = new TbOrderItem();
        tbOrderItem.setSellerId(tbItem.getSellerId());
        tbOrderItem.setGoodsId(tbItem.getGoodsId());
        tbOrderItem.setItemId(tbItem.getId());
        tbOrderItem.setNum(num);
        tbOrderItem.setPrice(tbItem.getPrice());
        tbOrderItem.setPicPath(tbItem.getImage());
        tbOrderItem.setTitle(tbItem.getTitle());
        tbOrderItem.setTotalFee(new BigDecimal(tbItem.getPrice().doubleValue()*num));

        return  tbOrderItem;
    }

    /**
     * 查询商家在cookie中购物车是否存在
     * @param cartList
     * @param sellerId
     * @return
     */
    private Cart searchCartBySellerId(List<Cart> cartList,String sellerId){

        //循环遍历查询 商家是否存在
        for (Cart cart:cartList){
            if (sellerId.equals(cart.getSellerId())){
                return  cart;
            }
        }
        return  null;
    }

    /**
     * 合并
     * @param cartList1
     * @param cartList2
     * @return
     */
    @Override
    public List<Cart> mergeCartList(List<Cart> cartList1, List<Cart> cartList2) {
        for (Cart cart:cartList1){
            for (TbOrderItem item:cart.getOrderItemList()){
                cartList2=addGoodsToCartList(cartList1,item.getItemId(),item.getNum());
            }
        }
        return cartList2;
    }
}
