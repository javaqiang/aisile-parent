package com.aisile.service;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author 要想成功, 必须强大!
 * @Title: SmsUtil
 * @ProjectName aisile-parent
 * @date 2018/9/3 0003
 */
@Component
public class SmsUtil {

    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";
    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
   /* static final String accessKeyId = "LTAIIQJgD2IIsDLL";                         //   SMS_143717641
    static final String accessKeySecret = "Xualwj4RAGen5bU4tcnO03L8Rz5V3f";  */     //   SMS_116560350

    @Autowired
    Environment environment;

    public  SendSmsResponse sendSms(Map<String,String> map) throws ClientException {
        String accessKeyId=environment.getProperty("accessKeyId");
        String accessKeySecret=environment.getProperty("accessKeySecret");
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();


        //必填:待发送手机号
        request.setPhoneNumbers(map.get("phone"));
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("八维云计算");
        //必填:短信模板-可在短信控制台中找到
        //"SMS_143717641"
        request.setTemplateCode(map.get("templateCode"));//SMS_143717641
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(map.get("param"));

        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("1234545");

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);

        return sendSmsResponse;
    }
}
