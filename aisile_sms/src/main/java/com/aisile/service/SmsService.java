package com.aisile.service;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author 要想成功, 必须强大!
 * @Title: SmsService
 * @ProjectName aisile-parent
 * @date 2018/9/3 0003
 */
@Component
public class SmsService {


    @Autowired
    SmsUtil smsUtil;

    /**
     *  手机 phone
     *  参数 param
     *  模板 code
     */
    @JmsListener(destination = "aisile-sms")
    public void readMsg(Map<String,String> map){
        //发短信
        try {
            SendSmsResponse response = smsUtil.sendSms(map);
            System.out.println("发送短信状态:"+response.getCode());
            System.out.println("发送的结果:"+response.getMessage());
        } catch (ClientException e) {
            e.printStackTrace();
            System.out.println("发送短信异常!");
        }
    }
}
