package com.aisile;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;

@SpringBootApplication
public class AisileSmsApplication {

	@Autowired
	Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(AisileSmsApplication.class, args);
	}

	@Bean
	public ConnectionFactory connectionFactory(){
		System.out.println("");
		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
		activeMQConnectionFactory.setBrokerURL(environment.getProperty("spring.activemq.broker-url"));
		activeMQConnectionFactory.setUserName(environment.getProperty("spring.activemq.user"));
		activeMQConnectionFactory.setPassword(environment.getProperty("spring.activemq.password"));
		return  activeMQConnectionFactory;
	}

	@Bean
	public JmsMessagingTemplate jmsMessagingTemplate(){
		return  new JmsMessagingTemplate(connectionFactory());
	}

	@Bean
	public JmsTemplate jmsTemplate(){
		return  new JmsTemplate(connectionFactory());
	}
}
