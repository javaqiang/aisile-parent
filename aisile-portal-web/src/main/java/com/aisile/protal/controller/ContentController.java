package com.aisile.protal.controller;

import com.aisile.content.service.ContentService;
import com.aisile.pojo.TbContent;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: ContentController
 * @ProjectName aisile-parent
 * @date 2018/8/17 0017
 */
@RestController
@RequestMapping("content")
public class ContentController {
    @Reference
    ContentService contentService;

    /**
     * 根据分类id查询广告集合
     * @param categoryId
     * @return
     */
    @RequestMapping("findByCatgoryId")
    public List<TbContent> findByCatgoryId(Long categoryId){
        return  contentService.findByCategoryId(categoryId);
    }
}
