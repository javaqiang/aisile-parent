//自定义品牌服务!
app.service("brandService",function ($http) {

    this.add=function (brand) {
        return $http.post('../brand/add.do',brand);
    };

    this.update=function (brand) {
        return $http.post('../brand/update.do',brand);
    };

    this.findOne=function (id) {
        return $http.get("../brand/findOne.do?id="+id);
    };

    this.delete=function (selectIds) {
        return $http.get('../brand/delete.do?ids='+selectIds);
    };
    this.search=function (pageNum,pageSize,searchEntity) {
        return $http.post('../brand/search.do?pageNum='+pageNum+'&pageSize='+pageSize,searchEntity);
    }
    //查询品牌下拉信息
    this.selectOptionList=function () {
        return $http.get("../brand/selectOptionList.do");
    }
});