app.controller('brandCtrl',function ($scope,brandService,$controller) {

    /*  与java继承关系类似                                    ↑*/
    $controller('baseController',{$scope:$scope});
    //搜索框
    $scope.searchEntity={};
    //获取数据
    $scope.search = function(pageNum,pageSize){
        brandService.search(pageNum,pageSize,$scope.searchEntity).success(function(result){
            $scope.list=result.rows;
            $scope.paginationConf.totalItems =result.total;
        });
    }

    //添加
    $scope.save=function () {
        if ($scope.brand.id!=null){
            brandService.update($scope.brand).success(
                function (response) {
                    //进行判断
                    if (response.success){
                        $scope.reloadList();
                    }else{
                        //失败 则打印信息
                        alert(response.message);
                    }
                }
            )
        }else{
            brandService.add($scope.brand).success(
                function (response) {
                    //进行判断
                    if (response.success){
                        $scope.reloadList();
                    }else{
                        //失败 则打印信息
                        alert(response.message);
                    }
                }
            )
        }

    }

    //根据id获取数据
    $scope.findOne=function (id) {
        brandService.findOne(id).success(
            function (response) {
                $scope.brand=response;
            }
        )
    }


    //删除
    $scope.dele=function () {
        swal({
            title : '确定删除吗？',
            text : '你将无法恢复它！',
            type : 'warning',
            showCancelButton : true,
            confirmButtonColor : '#3085d6',
            cancelButtonColor : '#d33',
            confirmButtonText : '确定！',
            cancelButtonText : '取消！',
            confirmButtonClass : 'btn btn-success',
            cancelButtonClass : 'btn btn-danger'
        }).then(function(isConfirm) {
            if (isConfirm.value == true) {
                swal('已删除！', '信息已经被删除。');
            }else{
                swal('删除失败！', '请稍后再试。', 'error');
            }
        })
        brandService.delete($scope.selectIds).success(
            function(response){
                if(response.success){
                    $scope.reloadList();//刷新列表
                }
            }
        )
    }

});