app.controller('baseController',function ($scope) {
    //分页控件配置
    $scope.paginationConf = {
        currentPage: 1,
        totalItems: 10,
        itemsPerPage: 10,
        perPageOptions: [10, 20, 30, 40, 50],
        onChange: function(){
            $scope.reloadList();//重新加载
        }
    };

    $scope.selectIds=[];//存放选择的id
    //更新
    $scope.updateSelection=function ($event,id) {
        if ($event.target.checked){//判断是否勾选了数据
            $scope.selectIds.push(id);//选中就添加
        }else{
            $scope.selectIds.splice($scope.selectIds.indexOf(id),1);//如果取消则就删除勾选时存的id，
            // 后面的1代表删除几个，固定写法默认为1，因为就算取消勾选也只能一个一个的取消
        }
    }

//刷新(上一页 下一页)
    $scope.reloadList = function(){
        $scope.search($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage);
    }
});