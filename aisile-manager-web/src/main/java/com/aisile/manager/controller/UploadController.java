package com.aisile.manager.controller;

/**
 * @author 要想成功, 必须强大!
 * @Title: UploadController
 * @ProjectName aisile-parent
 * @date 2018/8/14 0014
 */

import com.aisile.common.FastDFSClient;
import com.aisile.entry.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件上传
 */
@RestController
@RequestMapping("file")
public class UploadController {
    @Value("${IMAGE_SERVER_PATH}")
    private String IMAGE_SERVER_PATH;//服务器地址


    @RequestMapping("upload")
    public Map upload(MultipartFile file){
        Map<String,Object> map=new HashMap<>();
        try {
            String url = uploadFile(file);
            map.put("error",0);//当0时成功!
            map.put("url",url);
            return  map;
        }catch (Exception e){
            map.put("error",1); //唯一时 失败
            map.put("message","上传异常!");
            return  map;
        }

    }


  @RequestMapping("uploadAngular")
    public Result uploadAngular(MultipartFile file){
        try {
            String url = uploadFile(file);
            return  new Result(true,url);
        }catch (Exception e){

            return  new Result(false,"上传异常");
        }
    }

    private  String uploadFile(MultipartFile file) throws  Exception{
        String fileName=file.getOriginalFilename();
        //获取后缀名
        String suffix = fileName.substring(fileName.lastIndexOf(".")+1);
        //创建客户端
        FastDFSClient fastDFSClient=new FastDFSClient("classpath:config/client.conf");
        String url = fastDFSClient.uploadFile(file.getBytes(), suffix);
        return  IMAGE_SERVER_PATH+url;
    }
}
