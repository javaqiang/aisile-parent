package com.aisile.manager.controller;

import com.aisile.entry.PageResult;
import com.aisile.entry.Result;
import com.aisile.pojo.TbBrand;
import com.aisile.sellergoods.service.BrandService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author 要想成功, 必须强大!
 * @Title: BarndController
 * @ProjectName aisile-parent
 * @date 2018/8/3 0003
 */
@Controller
@RequestMapping("brand")
public class BarndController {

    @Reference
    BrandService brandService;


    @RequestMapping("search")
    @ResponseBody
    public PageResult findPage(
            @RequestParam(defaultValue = "1" ,value = "pageNum") Integer  pageNum,
            @RequestParam(defaultValue = "10" ,value = "pageSize") Integer pageSize,@RequestBody TbBrand tbBrand){
        return brandService.findPage(tbBrand,pageNum,pageSize);
    }


    /**
     * 品牌添加
     * @param tbBrand
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(@RequestBody TbBrand tbBrand){
        try {
            brandService.add(tbBrand);
            return  new Result(true,"增加成功!");
        }catch (Exception e){
            e.printStackTrace();
            return  new Result(false,"增加失败!");
        }
    }

    /**
     * 根据id  查询实体类
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    @ResponseBody
    public TbBrand findOne(long id){
        return  brandService.findOne(id);
    }

    /**
     * 根据 编号 修改品牌
     * @param tbBrand
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(@RequestBody TbBrand tbBrand){
        try {
            brandService.update(tbBrand);
            return new Result(true,"修改成功!");
        }catch (Exception e){
            e.printStackTrace();
            return  new Result(false,"修改失败!");
        }
    }

    /**
     * 根据 id 删除
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public  Result delete( Long [] ids){
        try {
            brandService.delete(ids);
            return  new Result(true,"删除成功!");
        }catch (Exception e){
            e.printStackTrace();
            return  new Result(false,"删除失败!");
        }
    }

    @RequestMapping("/selectOptionList")
    @ResponseBody
    public List<Map> selectOptionList(){
        return  brandService.selectOptionList();
    }
}
