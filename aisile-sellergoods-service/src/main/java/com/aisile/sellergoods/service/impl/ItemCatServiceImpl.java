package com.aisile.sellergoods.service.impl;
import java.util.List;
import java.util.Map;

import com.aisile.common.Constant;
import com.aisile.entry.PageResult;
import com.aisile.mapper.TbTypeTemplateMapper;
import com.aisile.pojo.TbItemCatExample;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.aisile.mapper.TbItemCatMapper;
import com.aisile.pojo.TbItemCat;
import com.aisile.sellergoods.service.ItemCatService;
import org.springframework.data.redis.core.RedisTemplate;


/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class ItemCatServiceImpl implements ItemCatService {

	@Autowired
	private TbItemCatMapper itemCatMapper;

	@Autowired
	private TbTypeTemplateMapper tbTypeTemplateMapper;
	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbItemCat> findAll() {
		return itemCatMapper.selectByExample(null);
	}

	@Autowired
	RedisTemplate redisTemplate;
	/**
	 * 按分页查询
	 */
	@Override
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<TbItemCat> page=   (Page<TbItemCat>) itemCatMapper.selectByExample(null);
		//只要数据改变就重新引入
		return new PageResult(page.getTotal(), page.getResult());
	}

	/**
	 * 存储的是: 模板id
	 * redis: hash
	 */

	private void cacheItemCatTemplate(){
		List<TbItemCat> list=findAll();
		for (TbItemCat itemCat:list){
			//分类列表 和模板id
			redisTemplate.boundHashOps(Constant.ITEMCAT_TEMPLATE_CATE).put(itemCat.getName(),itemCat.getTypeId());
		}
		System.out.println("----------缓存分类模板id数据成功!---------------");
	}

	/**
	 * 增加
	 */
	@Override
	public void add(TbItemCat itemCat) {
		itemCatMapper.insert(itemCat);		
	}

	
	/**
	 * 修改
	 */
	@Override
	public void update(TbItemCat itemCat){
		itemCatMapper.updateByPrimaryKey(itemCat);
	}	
	
	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	public TbItemCat findOne(Long id){
		return itemCatMapper.selectByPrimaryKey(id);
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
			itemCatMapper.deleteByPrimaryKey(id);
		}		
	}
	
	
		@Override
	public PageResult findPage(TbItemCat itemCat, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		
		TbItemCatExample example=new TbItemCatExample();
		TbItemCatExample.Criteria criteria = example.createCriteria();
		
		if(itemCat!=null){			
						if(itemCat.getName()!=null && itemCat.getName().length()>0){
				criteria.andNameLike("%"+itemCat.getName()+"%");
			}
	
		}
		
		Page<TbItemCat> page= (Page<TbItemCat>)itemCatMapper.selectByExample(example);		
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public List<TbItemCat> findByParentId(Long parentId) {
		TbItemCatExample example=new TbItemCatExample();
		TbItemCatExample.Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		cacheItemCatTemplate();//这是缓存在 redis 中 json数组
		return itemCatMapper.selectByExample(example);
	}

	@Override
	public List<Map> selectOptionList() {
		return itemCatMapper.selectOptionList();
	}


}
