package com.aisile.sellergoods.service.impl;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.aisile.entry.PageResult;
import com.aisile.mapper.*;
import com.aisile.pojo.*;
import com.aisile.pojogroup.Goods;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.aisile.sellergoods.service.GoodsService;


/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class GoodsServiceImpl implements GoodsService {

	@Autowired
	private TbGoodsMapper goodsMapper;

	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbGoods> findAll() {
		return goodsMapper.selectByExample(null);
	}

	/**
	 * 按分页查询
	 */
	@Override
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);		
		Page<TbGoods> page=   (Page<TbGoods>) goodsMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}

	/**
	 * 增加
	 */
	@Override
	public void add(TbGoods goods) {
		goodsMapper.insert(goods);		
	}

	
	/**
	 * 修改
	 */
	@Override
	public void update(Goods goods){
		goodsMapper.updateByPrimaryKey(goods.getGoods());//修改商品表
        tbGoodsDescMapper.updateByPrimaryKey(goods.getGoodsDesc());//修改商品详情表
        //得到条件 对sku进行操作
        TbItemExample example=new TbItemExample();
        //得到商品条件
        example.createCriteria().andGoodsIdEqualTo(goods.getGoods().getId());
        itemMapper.deleteByExample(example);
        //修改sku
        addItemList(goods);
	}	


	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	public Goods findOne(Long id){
        Goods goods=new Goods();
        TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
        goods.setGoods(tbGoods);
        TbGoodsDesc tbGoodsDesc = tbGoodsDescMapper.selectByPrimaryKey(id);
        goods.setGoodsDesc(tbGoodsDesc);
        //根据id查询sku表
        TbItemExample example=new TbItemExample();
        example.createCriteria().andGoodsIdEqualTo(id);
        List<TbItem> itemList = itemMapper.selectByExample(example);
        goods.setItemList(itemList);
        return goods;
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
		    //现根据id查询商品
            TbGoods goods = goodsMapper.selectByPrimaryKey(id);
            //设置以删除
            goods.setIsDelete("1");
            //修改
            goodsMapper.updateByPrimaryKey(goods);
            //goodsMapper.deleteByPrimaryKey(id);
		}		
	}
	
	
		@Override
	public PageResult findPage(TbGoods goods, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		TbGoodsExample example=new TbGoodsExample();
		TbGoodsExample.Criteria criteria = example.createCriteria();
		if(goods!=null){			
			if(goods.getSellerId()!=null && goods.getSellerId().length()>0){
				criteria.andSellerIdEqualTo(goods.getSellerId());
			}
			if(goods.getGoodsName()!=null && goods.getGoodsName().length()>0){
				criteria.andGoodsNameLike("%"+goods.getGoodsName()+"%");
			}
			if(goods.getAuditStatus()!=null && goods.getAuditStatus().length()>0){
				criteria.andAuditStatusEqualTo(goods.getAuditStatus());
			}
			if(goods.getIsMarketable()!=null && goods.getIsMarketable().length()>0){
				criteria.andIsMarketableLike("%"+goods.getIsMarketable()+"%");
			}
			if(goods.getCaption()!=null && goods.getCaption().length()>0){
				criteria.andCaptionLike("%"+goods.getCaption()+"%");
			}
			if(goods.getSmallPic()!=null && goods.getSmallPic().length()>0){
				criteria.andSmallPicLike("%"+goods.getSmallPic()+"%");
			}
			if(goods.getIsEnableSpec()!=null && goods.getIsEnableSpec().length()>0){
				criteria.andIsEnableSpecLike("%"+goods.getIsEnableSpec()+"%");
			}
			if(goods.getIsDelete()!=null && goods.getIsDelete().length()>0){
				criteria.andIsDeleteLike("%"+goods.getIsDelete()+"%");
			}
	
		}
		//增加条件 根据删除的状态 1为删除
	 	//criteria.andIsDeleteNotEqualTo("1");
		criteria.andIsDeleteIsNull();
		Page<TbGoods> page= (Page<TbGoods>)goodsMapper.selectByExample(example);		
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Autowired
	TbGoodsDescMapper tbGoodsDescMapper;
	@Autowired
	TbSellerMapper sellerMapper;//商家的
	@Autowired
	TbBrandMapper brandMapper;//品牌的
	@Autowired
	TbItemCatMapper itemCatMapper;//分类的
	@Autowired
	TbItemMapper itemMapper;//sku表

	/**
	 * 添加商品
	 * @param goods
	 */
	@Override
	public void add(Goods goods) {
		//先添加spu信息 返回id;
		TbGoods tbGoods=goods.getGoods();
		TbGoodsDesc goodsDesc=goods.getGoodsDesc();
		tbGoods.setAuditStatus("0");
		goodsMapper.insert(tbGoods);
		//将生成的id给详情表
		goodsDesc.setGoodsId(tbGoods.getId());
		tbGoodsDescMapper.insert(goodsDesc);
		addItemList(goods);

	}

	/**
	 * 更改商品状态
	 * @param ids 品牌的信息
	 * @param statuts 状态
	 */
	public void updateStatus(Long [] ids,String statuts){
		for (Long id:ids){
			//首先根据商品编号查询到商品
			TbGoods goods = goodsMapper.selectByPrimaryKey(id);
			//给商品的状态赋值
			goods.setAuditStatus(statuts);
			//修改品牌的状态
			goodsMapper.updateByPrimaryKey(goods);
		}
	}

	/**
	 * 添加 sku
	 * @param goods
	 */
	private void addItemList(Goods goods){
        //获取商家的名称
        TbSeller seller=sellerMapper.selectByPrimaryKey(goods.getGoods().getSellerId());
        //获取品牌的名称
        TbBrand brand=brandMapper.selectByPrimaryKey(goods.getGoods().getBrandId());
        //这是获取分类名字
        TbItemCat ItemCat = itemCatMapper.selectByPrimaryKey(goods.getGoods().getCategory3Id());
        //图片
        List<Map> listMap = JSON.parseArray(goods.getGoodsDesc().getItemImages(),Map.class);
        //判断规格是否启用
        if ("1".equals(goods.getGoods().getIsEnableSpec())){
            //保存sku库存信息
            List<TbItem> itemList=goods.getItemList();
            for (TbItem item:itemList){
                String title=goods.getGoods().getGoodsName();
                Map<String,Object> map= JSON.parseObject(item.getSpec());//将数组转化为字符串
                for (String key:map.keySet()){
                    title= title+" "+map.get(key);//循环便利
                }
                item.setTitle(title);
                item.setCategory(ItemCat.getName());//得到商品分类的名字
                item.setCategoryid(goods.getGoods().getCategory3Id());
                item.setGoodsId(goods.getGoods().getId());
                item.setImage(listMap.size()>0?listMap.get(0).toString():"");//得到图片
                item.setSellerId(seller.getSellerId());
                item.setSeller(seller.getNickName());//这是卖家的昵称
                item.setBrand(brand.getName());
                item.setCreateTime(new Date());//创建的时间
                item.setUpdateTime(new Date());//修改的时间
                itemMapper.insert(item);
            }
        }else{
            TbItem item=new TbItem();
            String title=goods.getGoods().getGoodsName();
            item.setTitle(title);
            item.setCategory(ItemCat.getName());//得到商品分类的名字
            item.setCategoryid(goods.getGoods().getCategory3Id());
            item.setGoodsId(goods.getGoods().getId());
            item.setImage(listMap.size()>0?listMap.get(0).toString():"");//得到图片
            item.setSellerId(seller.getSellerId());
            item.setSeller(seller.getNickName());//这是卖家的昵称
            item.setBrand(brand.getName());
            item.setCreateTime(new Date());//创建的时间
            item.setUpdateTime(new Date());//修改的时间
            item.setPrice(goods.getGoods().getPrice());
            item.setNum(999);
            item.setStatus("1");
            item.setIsDefault("1");
            itemMapper.insert(item);
        }
    }


	/**
	 * 根据 spu的id 查询sku的集合
	 * @param goodsIds spuId
	 * @param status
	 * @return
	 */
    public List<TbItem> findItemListByGoodsIdListAndStatus(Long [] goodsIds,String status){

		TbItemExample example=new TbItemExample();
		com.aisile.pojo.TbItemExample.Criteria criteria = example.createCriteria();
		criteria.andGoodsIdIn(Arrays.asList(goodsIds));
		criteria.andStatusEqualTo(status);
		List<TbItem> itemList = itemMapper.selectByExample(example);
		System.out.println(itemList.size());
		return itemList;
	}

}
