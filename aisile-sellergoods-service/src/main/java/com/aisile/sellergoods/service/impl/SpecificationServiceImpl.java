package com.aisile.sellergoods.service.impl;
import java.util.List;
import java.util.Map;

import com.aisile.entry.PageResult;
import com.aisile.mapper.TbSpecificationOptionMapper;
import com.aisile.pojo.TbSpecificationExample;
import com.aisile.pojo.TbSpecificationOption;
import com.aisile.pojo.TbSpecificationOptionExample;
import com.aisile.pojogroup.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.aisile.mapper.TbSpecificationMapper;
import com.aisile.pojo.TbSpecification;
import com.aisile.sellergoods.service.SpecificationService;


/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class SpecificationServiceImpl implements SpecificationService {

	@Autowired
	private TbSpecificationMapper specificationMapper;

	@Autowired
	private TbSpecificationOptionMapper tbSpecificationOptionMapper;
	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbSpecification> findAll() {
		return specificationMapper.selectByExample(null);
	}

	/**
	 * 按分页查询
	 */
	@Override
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);		
		Page<TbSpecification> page=   (Page<TbSpecification>) specificationMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}

	/**
	 * 增加
	 */
	@Override
	public void add(Specification specification) {
		TbSpecification tbSpecification=specification.getSpecification();
		//添加规格
		specificationMapper.insert(tbSpecification);
		List<TbSpecificationOption> tbSpecificationOptions=specification.getSpecificationOptionList();
		//根据生成的id 添加规格选项表
		for (TbSpecificationOption option:tbSpecificationOptions){
			option.setSpecId(tbSpecification.getId());
			tbSpecificationOptionMapper.insert(option);
		}
	}

	
	/**
	 * 修改
	 */
	@Override
	public void update(Specification specification){
	    TbSpecification tbSpecification=specification.getSpecification();
	    List<TbSpecificationOption> options=specification.getSpecificationOptionList();
        //修改规格
        specificationMapper.updateByPrimaryKey(tbSpecification);
	    //删除规格选项 和条件
        TbSpecificationOptionExample example=new TbSpecificationOptionExample();
        example.createCriteria().andSpecIdEqualTo(tbSpecification.getId());
		tbSpecificationOptionMapper.deleteByExample(example);
        //添加规格选项
        for (TbSpecificationOption option:options){
            option.setSpecId(tbSpecification.getId());
            tbSpecificationOptionMapper.insert(option);
        }

	}	
	
	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	public Specification findOne(Long id){
	    TbSpecification tbSpecification=specificationMapper.selectByPrimaryKey(id);
        TbSpecificationOptionExample example=new TbSpecificationOptionExample();
        //获取条件
         example.createCriteria().andSpecIdEqualTo(id);
	    List<TbSpecificationOption> options=tbSpecificationOptionMapper.selectByExample(example);
        Specification specification=new Specification();
        specification.setSpecification(tbSpecification);
        specification.setSpecificationOptionList(options);
		return specification;
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
			specificationMapper.deleteByPrimaryKey(id);
            //删除规格选项 和条件
            TbSpecificationOptionExample example=new TbSpecificationOptionExample();
            example.createCriteria().andSpecIdEqualTo(id);
            tbSpecificationOptionMapper.deleteByExample(example);
		}		
	}
	
	
		@Override
	public PageResult findPage(TbSpecification specification, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		
		TbSpecificationExample example=new TbSpecificationExample();
		TbSpecificationExample.Criteria criteria = example.createCriteria();
		
		if(specification!=null){			
						if(specification.getSpecName()!=null && specification.getSpecName().length()>0){
				criteria.andSpecNameLike("%"+specification.getSpecName()+"%");
			}
	
		}
		
		Page<TbSpecification> page= (Page<TbSpecification>)specificationMapper.selectByExample(example);		
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public List<Map> selectOptionList() {
		return specificationMapper.selectOptionList();
	}
}
