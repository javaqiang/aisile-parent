app.controller('searchController',function($scope,$location,searchService){
    $scope.searchMap={'keywords':'','category':'','brand':'','spec':{},'price':'','pageNo':'1','pageSize':'40','sort':'','sortField':''};//搜索对象
    //                 高光↑          商品分类  ↑   品牌  ↑   规格 对象 ↑     价格↑  当前页↑   显示  ↑
    //搜索
    $scope.search=function(){
        //如果点击的是分类或者是品牌 搜索框
        searchService.search( $scope.searchMap ).success(
            function(response){
                $scope.resultMap=response;//搜索返回的结果
                //console.log($scope.resultMap);
                buildPageLabel();//分页栏
            }
        );

    }

    buildPageLabel=function () {
        //分页栏
        debugger;
        $scope.pageLabel=[];//新增分页栏属性
        var maxPageNo= $scope.resultMap.totalPages;//得到最后页码
        var firstPage=1;//开始页码
        var lastPage=maxPageNo;//截止页码

       $scope.firstDot=true;//前面有点
        $scope.lastDot=true;//后面有点

        if($scope.resultMap.totalPages> 5){  //如果总页数大于5页,显示部分页码
            if($scope.searchMap.pageNo<=3){//如果当前页小于等于3
                lastPage=5; //前5页
                $scope.firstDot=false;
            }else if( $scope.searchMap.pageNo>=lastPage-2  ){//如果当前页大于等于最大页码-2
                firstPage= maxPageNo-4;		 //后5页
                $scope.lasetDot=false;
            }else{ //显示当前页为中心的5页
                firstPage=$scope.searchMap.pageNo-2;
                lastPage=$scope.searchMap.pageNo+2;
            }
        } else{
            $scope.firstDot=false;//前面有点
            $scope.lastDot=false;//后面有点
        }
        //循环产生页码标签
        for(var i=firstPage;i<=lastPage;i++){
            $scope.pageLabel.push(i);
        }
    }

    //点击按钮进行查询
    $scope.addSearchItem=function (key,val) {

        if (key=='category'||key=='brand'|| key=='price'){
            $scope.searchMap[key]=val;
        }else{
            $scope.searchMap.spec[key]=val;
        }
      $scope.search();
    }


    //移除方法
    $scope.removeSearchItem=function (key) {
       // debugger
        if (key=='category'||key=='brand'|| key=='price'){
            $scope.searchMap[key]="";
        }else{
            delete $scope.searchMap.spec[key];
        }
       $scope.search();
    }

    //分页查询
    $scope.queryByPage=function (pageNo) {//获取的是当前页的页数
        //加入判断 当前页
        if(pageNo<1||pageNo>$scope.searchMap.totalPrices){
            return ;
        }
        debugger;
        $scope.searchMap.pageNo=pageNo;
        $scope.search();
    }

    //判断当前页是否为第一页
    $scope.isTopPage=function () {
        if ($scope.searchMap.pageNo==1){
            return true;
        }else{
            return false;
        }
    }

    //判断是否为最后一页
    $scope.isEndPage=function () {
        if ($scope.searchMap.pageNo==$scope.searchMap.totalPrices){
            return true;
        }else{
            return false;
        }
    }

    //排序
    $scope.sortSearch=function (sortField,sort) {
        $scope.searchMap.sortField=sortField;
        $scope.searchMap.sort=sort;
        $scope.search();
    }

    //更具搜索的名字 来判断是不是品牌  如果是
    $scope.keywordsIsBrand=function () {
        for(var i=0;i<$scope.resultMap.brandList.length;i++){
            if($scope.searchMap.keywords.indexOf($scope.resultMap.brandList[i].text)>=0){//如果包含
                return true;
            }
        }
        return false;
    }

    //加载关键字
    $scope.locadkeywords=function () {
        $scope.searchMap.keywords= $scope.search()['keywords'];
        $scope.search();
    }

});