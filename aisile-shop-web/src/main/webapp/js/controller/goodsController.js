 //控制层 
app.controller('goodsController' ,function($scope,$controller,$location   ,typeTemplateService,itemCatService,goodsService,uploadService){
	
	$controller('baseController',{$scope:$scope});//继承

    //          商品信息 ↓  商品属性配置表↓    图片↓    规格 ↓
    $scope.entity={goods:{},goodsDesc:{itemImages:[],specificationItems:[],itemList:[]}};
	
    //读取列表数据绑定到表单中  
	$scope.findAll=function(){
		goodsService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}    
	
	//分页
	$scope.findPage=function(page,rows){
		debugger;
		goodsService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//查询实体 
	$scope.findOne=function(){
		var id=$location.search()['id'];
		if(id){
            goodsService.findOne(id).success(
                function(response){
                    $scope.entity= response;
					editor.html($scope.entity.goodsDesc.introduction);
					$scope.entity.goodsDesc.itemImages=JSON.parse($scope.entity.goodsDesc.itemImages);
					$scope.entity.goodsDesc.specificationItems=JSON.parse($scope.entity.goodsDesc.specificationItems);
					$scope.entity.goodsDesc.customAttributeItems=JSON.parse($scope.entity.goodsDesc.customAttributeItems);
                }
            );
        }
	}
	
	//保存 
	$scope.save=function(){				
		var serviceObject;//服务层对象
        $scope.entity.goodsDesc.introduction=editor.html();
		if($scope.entity.goods.id!=null){//如果有ID
			serviceObject=goodsService.update( $scope.entity ); //修改  
		}else{
			serviceObject=goodsService.add( $scope.entity  );//增加 
		}				
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
		        	location.href="goods.html";
				}else{
					alert(response.message);
				}
			}		
		);				
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		goodsService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
					$scope.selectIds=[];
				}						
			}		
		);				
	}
	
	$scope.searchEntity={};//定义搜索对象 
	
	//搜索
	$scope.search=function(page,rows){			
		goodsService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}

	$scope.add=function(){
		$scope.entity.goodsDesc.introduction=editor.html();
        goodsService.add($scope.entity).success(
        	function(response){
        	    if(response.success){
        	        alert("添加成功!");
                    $scope.entity={};
                    editor.html('');
                }else{
        	        alert(response.message);
                }
            }
		)
	}

	$scope.image_entity={};
	//s上传的方法
	$scope.upload_image_entity=function () {
        uploadService.uploadFile().success(
        	function (result) {
				if (result.success){
					//上传成功
                    $scope.image_entity.url=result.message;
				}else{
					//答应错误信息
					alert(result.message);
				}
            }
		)
    }
    //保存图片的方法
	$scope.add_entity_image=function () {
		$scope.entity.goodsDesc.itemImages.push($scope.image_entity);
    }
    //删除图片
    $scope.del_image_file=function (index) {
        var obj=$scope.entity.goodsDesc.itemImages.splice(index,1);

    }

    //分类1
	$scope.itemCat1List=[];
    //分类2
    $scope.itemCat1List=[];
    //分类3
    $scope.itemCat1List=[];
    //分类1初始化
    $scope.findItemCat1list=function () {
		itemCatService.findParentId(0).success(
			function (result) {
                $scope.itemCat1List=result;
            }
		)
    }
    //监控商品分类1 查的是属性item_cat商品分类表
	$scope.$watch('entity.goods.category1Id',function (newVal,oldVal) {
		if (newVal){
			itemCatService.findParentId(newVal).success(
				function (result) {
                    $scope.itemCat2List=result;
                    $scope.itemCat3List=[];
                }
			)
		}else{
            $scope.itemCat2List=[];
		}
    });
    //监控分类2 查的是属性item_cat商品分类表
    $scope.$watch('entity.goods.category2Id',function (newVal,oldVal) {
        if (newVal){
            itemCatService.findParentId(newVal).success(
                function (result) {
                    $scope.itemCat3List=result;
                }
            )
        }else{
            $scope.itemCat3List=[];
		}
    });
    //监控分类3 查的是属性item_cat商品分类表
    $scope.$watch('entity.goods.category3Id',function (newVal,oldVal) {
        if (newVal){
        	//查的是属性item_cat表
        	itemCatService.findOne(newVal).success(
        		function (result) {
        			//查询商品表 得到模板表id
					$scope.entity.goods.typeTemplateId=result.typeId;

                }
			)
        }
    });

    //监控模板id  查询模板表
    $scope.specList=[];//存放规格
	$scope.$watch('entity.goods.typeTemplateId',function (newVal,oldVal) {
       if (newVal){
       	typeTemplateService.findOne(newVal).success(
       		function (result) {
				$scope.typeTemplate=result;
                $scope.typeTemplate.brandIds=JSON.parse($scope.typeTemplate.brandIds);
                if(!$location.search()['id']){
                    $scope.entity.goodsDesc.customAttributeItems=JSON.parse($scope.typeTemplate.customAttributeItems);
				}
				//做修改的时候 会显 规格
				for (var i in $scope.entity.itemList){
                    $scope.entity.itemList[i].spec=JSON.parse($scope.entity.itemList[i].spec);
				}
            }
		)};

	   typeTemplateService.findSpecList(newVal).success(function (result) {
			$scope.specList=result;
	   });

	});

    $scope.updateSpecAttribute=function($event,name,value){
        var object= $scope.searchObjByKey($scope.entity.goodsDesc.specificationItems ,'attributeName', name);
        if(object!=null){//判断key是否存在!
            if($event.target.checked ){
                //如果选中了就添加
                object.attributeValue.push(value);
            }else{//取消勾选 就移除
				object.attributeValue.splice( object.attributeValue.indexOf(value ) ,1);//移除选项
                //如果选项都取消了，将此条记录移除
                if(object.attributeValue.length==0){
                    $scope.entity.goodsDesc.specificationItems.splice(
                        $scope.entity.goodsDesc.specificationItems.indexOf(object),1);//这是规格信息
                }

            }
        }else{
            $scope.entity.goodsDesc.specificationItems.push(
                {"attributeName":name,"attributeValue":[value]});
        }
    }

    //sku的属性 也就是库存表
    $scope.createSkuItemList=function () {
    	//创建一个默认的sku表
		$scope.entity.itemList=[{num:999,price:0,status:'0',idDefault:'0',spec:{}}];
		//获取当前页面选择的规格
		var items=$scope.entity.goodsDesc.specificationItems;
		for (var i=0;i<items.length;i++){//循环有规格
            $scope.entity.itemList=addColumn($scope.entity.itemList,items[i]);
		}
    }
    addColumn=function (list,item) {
    	debugger;
        var newList=[];//新的集合
        for(var i=0;i<list.length;i++){
            var oldRow= list[i];//将原先的默认值给他
            for(var j=0;j<item.attributeValue.length;j++){
                var newRow= JSON.parse( JSON.stringify( oldRow )  );//深克隆 相当于复制了一个oldRow
                newRow.spec[item.attributeName]=item.attributeValue[j];//给他添加属性
                newList.push(newRow);//添加数组
            }
        }
        return newList;
    }

    //一个数组存放 审核状态
	$scope.state=['待审核','审核通过','以驳回'];

    //一个存放商品分类的数组
	$scope.itemArray=[]; //键为id 值为name
	//获取分类的值
	$scope.findItemCat=function () {
		itemCatService.findAll().success(function (result) {
			for (var i in result){
                $scope.itemArray[result[i].id]=result[i].name;
			}
        })
		
    }

    //根据规格名称 及值 查询是否存在 勾选规格
	$scope.checkHasThisVal=function (specName,optName) {
		//拿到规格信息
		var items=$scope.entity.goodsDesc.specificationItems;
		//判断指定的规格信息 是否存在
		var obj = $scope.searchObjByKey(items,'attributeName',specName);
		if (obj!=null){
			for (var i in obj.attributeValue){//这是一数组
				if (optName==obj.attributeValue[i]){
					return true;
				}
			}
			return false;
		}else{
			return false;
		}
    }

});	
