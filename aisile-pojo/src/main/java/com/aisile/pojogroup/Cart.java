package com.aisile.pojogroup;

import com.aisile.pojo.TbOrderItem;

import java.io.Serializable;
import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: Cart
 * @ProjectName aisile-parent
 * @date 2018/9/5 0005
 */
public class Cart  implements Serializable{

    private String sellerId;//存放商家id
    private String sellerName; //商家名称
    private List<TbOrderItem> orderItemList;//购物车

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public List<TbOrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<TbOrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }
}
