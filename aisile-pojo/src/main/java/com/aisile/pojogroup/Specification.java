package com.aisile.pojogroup;

import com.aisile.pojo.TbSpecification;
import com.aisile.pojo.TbSpecificationOption;

import java.io.Serializable;
import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: Specification
 * @ProjectName aisile-parent
 * @date 2018/8/7 0007
 */
public class Specification implements Serializable{
    private TbSpecification specification;

    private List<TbSpecificationOption> specificationOptionList;

    public Specification() {
    }

    public Specification(TbSpecification specification, List<TbSpecificationOption> specificationOptionList) {
        this.specification = specification;
        this.specificationOptionList = specificationOptionList;
    }

    public TbSpecification getSpecification() {
        return specification;
    }

    public void setSpecification(TbSpecification specification) {
        this.specification = specification;
    }

    public List<TbSpecificationOption> getSpecificationOptionList() {
        return specificationOptionList;
    }

    public void setSpecificationOptionList(List<TbSpecificationOption> specificationOptionList) {
        this.specificationOptionList = specificationOptionList;
    }
}
