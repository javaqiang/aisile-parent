package com.aisile.pojogroup;

import com.aisile.pojo.TbGoods;
import com.aisile.pojo.TbGoodsDesc;
import com.aisile.pojo.TbItem;

import java.io.Serializable;
import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: Goods
 * @ProjectName aisile-parent
 * @date 2018/8/13 0013
 */
public class Goods implements Serializable{
    //商品spu信息
    private TbGoods goods;
    //商品sku信息
    private TbGoodsDesc goodsDesc;
    //商品sku信息
    private List<TbItem> itemList;

    public TbGoods getGoods() {
        return goods;
    }

    public void setGoods(TbGoods goods) {
        this.goods = goods;
    }

    public TbGoodsDesc getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(TbGoodsDesc goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public List<TbItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<TbItem> itemList) {
        this.itemList = itemList;
    }
}
