package com.aisile.entry;

import java.io.Serializable;
import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: PageResult
 * @ProjectName aisile-parent
 * @date 2018/8/4 0004
 */
public class PageResult implements Serializable{
    /**
     * 分页
     */
    private long total; //数量

    private List rows; //数据

    public PageResult(long total, List rows) {
        this.total = total;
        this.rows = rows;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}
