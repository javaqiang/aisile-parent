package com.aisile.entry;

import java.io.Serializable;

/**
 * @author 要想成功, 必须强大!
 * @Title: Result
 * @ProjectName aisile-parent
 * @date 2018/8/5 0005
 */
public class Result implements Serializable{

    private  boolean success;//结果

    private  String message;

    public Result(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
