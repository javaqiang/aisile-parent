package com.aisile.page.service;

/**
 * @author 要想成功, 必须强大!
 * @Title: ItemPageService
 * @ProjectName aisile-parent
 * @date 2018/8/29 0029
 */
public interface ItemPageService {
    /**
     * 生成商品详细页
     */
    public boolean genItemHtml(Long goodsId);

    /**
     * 删除商品详情页
     */
    public boolean deleteItemHtml(Long [] goodsIds);
}
