package com.aisile.cart.controller;

import com.aisile.cart.service.CartService;
import com.aisile.common.Constant;
import com.aisile.entry.Result;
import com.aisile.pojogroup.Cart;
import com.aisile.utils.CookieUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: CartController
 * @ProjectName aisile-parent
 * @date 2018/9/5 0005
 */
@RestController
@RequestMapping("cart")
public class CartController {

    @Reference(timeout = 6000)
    CartService cartService;

    @Autowired
    private  HttpServletRequest request;

    @Autowired
    private  HttpServletResponse response;


    /**
     * 添加商品到购物车
     * @param request
     * @param response
     * @param itemId
     * @param num
     * @return
     */
    @RequestMapping("/addGoodsToCartList")
    public Result addGoodsToCartList(Long itemId,Integer num){
        //获取登陆人
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        try {
           //去除购物车
           List<Cart> cartList = findCartList();
           cartList= cartService.addGoodsToCartList(cartList, itemId, num);
            if("anonymousUser".equals(username)) { //如果是没有登录 去cookiez中取{
                CookieUtil.setCookie(request, response, Constant.CART_COOKIE_NAME, JSON.toJSONString(cartList), 24 * 3600, "utf-8");
                //这是向cookie中存
                System.out.println("存储cookie");
            }else{
                cartService.saveCartListToRedis(username,cartList);
            }
           return  new Result(true,"添加成功!");
       }catch (Exception e){
           e.printStackTrace();
           return  new Result(false,"添加失败!");
       }
    }


    /**
     * 购物车列表
     * @param request
     * @return
     */
    @RequestMapping("/findCartList")
    public List<Cart> findCartList(){
        //获取登陆人
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
         //如果是没有登录 去cookiez中取
            String cartListJsonStr  = CookieUtil.getCookieValue(request, Constant.CART_COOKIE_NAME, "utf-8");
            if (cartListJsonStr==null||"".equals(cartListJsonStr)){
                cartListJsonStr="[]";
            }
        List<Cart> carts_cookie = JSON.parseArray(cartListJsonStr, Cart.class);
        if("anonymousUser".equals(username)){
            return  carts_cookie;
        }else{ //如果登录了 去redis中查
            List<Cart> cartList_Redis = cartService.findCartListFromRedis(username);
            //如果cookie中的数据 大于0的时候
            if (carts_cookie.size()>0){
                //合并购物车
                 cartList_Redis = cartService.mergeCartList(cartList_Redis, carts_cookie);
                //清空cookie信息
                CookieUtil.deleteCookie(request,response,Constant.CART_COOKIE_NAME);
                //将合并后的数据放在 redis 中
                cartService.saveCartListToRedis(username,cartList_Redis);
            }
            return  cartList_Redis;
        }
    }

}
