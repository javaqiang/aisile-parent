//购物车控制层
app.controller('cartController',function($scope,cartService){


    //获取列表
    $scope.findCartList=function () {

        cartService.findCartList().success(function (result) {

                $scope.cartList=result;
                sum();

        })
    }

    //添加商品到购物车
    $scope.addGoodsToCartList=function(itemId,num){
        cartService.addGoodsToCartList(itemId,num).success(
            function(response){
                if(response.success){
                    $scope.findCartList();//刷新列表
                }else{
                    alert(response.message);//弹出错误提示
                }
            }
        );
    }

    sum=function () {
        $scope.totalNum=0;
        $scope.totalMoney=0;
        var carList=$scope.cartList;
        for (var i=0;i<carList.length;i++){
            var cart=carList[i];
            for (var j=0;j<cart.orderItemList.length;j++){
                var orderItem=cart.orderItemList[j];
                $scope.totalNum+=orderItem.num;
                $scope.totalMoney+=orderItem.totalFee;
            }
        }
    }
});