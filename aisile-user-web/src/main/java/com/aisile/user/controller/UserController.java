package com.aisile.user.controller;

import com.aisile.common.Constant;
import com.aisile.entry.PageResult;
import com.aisile.entry.Result;
import com.aisile.pojo.TbUser;
import com.aisile.user.service.UserService;
import com.aisile.utils.PhoneUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * controller
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Reference
	private UserService userService;
	
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbUser> findAll(){			
		return userService.findAll();
	}
	
	
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findPage")
	public PageResult findPage(int page, int rows){
		return userService.findPage(page, rows);
	}
	
	/**
	 * 增加 登陆的时候验证码判断
	 * @param user
	 * @return
	 */
	@RequestMapping("/add")
	public Result add(@RequestBody TbUser user,String smsCode){
		boolean checkSmsCode = userService.checkSmsCode(user.getPhone(), smsCode);
		if (checkSmsCode){
			try {
				userService.add(user);
				return new Result(true, "增加成功");
			} catch (Exception e) {
				e.printStackTrace();
				return new Result(false, "增加失败");
			}
		}else{
			return  new Result(false,"验证码错误!");
		}

	}
	
	/**
	 * 修改
	 * @param user
	 * @return
	 */
	@RequestMapping("/update")
	public Result update(@RequestBody TbUser user){
		try {
			userService.update(user);
			return new Result(true, "修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改失败");
		}
	}	
	
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public TbUser findOne(Long id){
		return userService.findOne(id);		
	}
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@RequestMapping("/delete")
	public Result delete(Long [] ids){
		try {
			userService.delete(ids);
			return new Result(true, "删除成功"); 
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}
	
		/**
	 * 查询+分页
	 * @param brand
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("/search")
	public PageResult search(@RequestBody TbUser user, int page, int rows  ){
		return userService.findPage(user, page, rows);		
	}




	@RequestMapping("sendCode")
	public Result sendCode(final String phone){
	    if (!PhoneUtils.isPhone(phone)){
	        return  new Result(false,"请输入正确的手机号!");
        }
		boolean falng=userService.sendCode(phone);
		return  new Result(falng,falng?"成功!":"失败");
	}


	@RequestMapping("loginName")
	public Map longinName(){
		Map map=new HashMap();
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		map.put("loginName",name);
		return  map;
	}
	
}
