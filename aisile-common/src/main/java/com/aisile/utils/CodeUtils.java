package com.aisile.utils;

import java.util.Random;

/**
 * @author 要想成功, 必须强大!
 * @Title: CodeUtils
 * @ProjectName aisile-parent
 * @date 2018/9/3 0003
 */
public class CodeUtils {

    /**
     * 生成验证码
     * @param num
     * @return
     */

    public static String getCode(int num){
        String str="0123456789";
        Random random=new Random();
        StringBuffer stringBuffer=new StringBuffer("");
        for (int i=0;i<num;i++){
            int index=random.nextInt(10);
            stringBuffer.append(str.charAt(index));
        }
        return  stringBuffer.toString();
    }
}
