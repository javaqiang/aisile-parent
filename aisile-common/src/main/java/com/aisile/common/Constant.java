package com.aisile.common;

/**
 * 常量类
 * @author 要想成功, 必须强大!
 * @Title: Constant
 * @ProjectName aisile-parent
 * @date 2018/8/21 0021
 */
public class Constant {

    //广告内容缓存
    public static  final  String CONTENT_CACHE="content_cache";

    //分类表+模板id
    public static  final String ITEMCAT_TEMPLATE_CATE="itemcat_template_cate";

    //模板 id +品牌列表
    public static  final  String TEMPLATE_BRAND_CACHE="template_brand_cache";

    //模板id +规格列表
    public static  final  String TEMPLATE_SPEC_CACHE="template_spec_cache";

    public static  final  String SMS_TEMPLATE_CODE_YZM="SMS_143717641";

    public static  final  int SMS_TEMPLATE_CODE_NUM=6;
    public static  final  String SMS_TEMPLATE_CODE_CACHE="sms_code_";

    public static  final  String CART_COOKIE_NAME="cartList";
}
