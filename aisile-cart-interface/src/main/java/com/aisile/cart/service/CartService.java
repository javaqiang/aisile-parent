package com.aisile.cart.service;

import com.aisile.pojogroup.Cart;

import java.util.List;

/**
 * @author 要想成功, 必须强大!
 * @Title: CartService  购物车的服务接口
 * @ProjectName aisile-parent
 * @date 2018/9/5 0005
 */
public interface CartService {


    /**
     * 添加商品到购物车
     * @param cartList  cookie 存的购物车信息
     * @param itemId sku
     * @param num   数量
     * @return
     */
    List<Cart> addGoodsToCartList(List<Cart> cartList,Long itemId,Integer num);


    /**
     * 从redis中查询购物车
     * @param username
     * @return
     */
    public List<Cart> findCartListFromRedis(String username);

    /**
     * 将购物车保存到redis
     * @param username
     * @param cartList
     */
    public void saveCartListToRedis(String username,List<Cart> cartList);

    /**
     * 合并购物车
     * @param cartList1
     * @param cartList2
     * @return
     */
    public List<Cart> mergeCartList(List<Cart> cartList1,List<Cart> cartList2);
}
