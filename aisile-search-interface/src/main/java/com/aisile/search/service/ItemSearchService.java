package com.aisile.search.service;

import java.util.List;
import java.util.Map;

/**
 * @author 要想成功, 必须强大!
 * @Title: ItemSearchService
 * @ProjectName aisile-parent
 * @date 2018/8/23 0023
 */
public interface ItemSearchService {


     /**
      * 搜索
      * @param searchMap
      * @return
      */
     Map<String,Object> search(Map searchMap);


     /**
      * 导入列表
      * @param list
      */
     void importList(List list);

     /**
      * 删除商品列表
      */
     public void deleteByGoodsIds(List goodsIds);
}
